<?php

namespace App\Draw;

use Doctrine\DBAL\Driver\PDOConnection;
use PDO;
use PDOException;

class Draw
{


    public function PDOconnect($user, $pass)
    {
        try {
            $db = new PDO('mysql:host=127.0.0.1:8889;dbname=twelve', $user, $pass);
            return $db;
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }

    }

    public function main()
    {

        // On se connecte à la BDD

        $user = 'root';
        $pass = 'root';

        $db = $this->PDOconnect($user, $pass);


        $queryItem = "SELECT * from item WHERE sold_tickets = nbre_tickets";

        $response = $db->query($queryItem);


        $itemIDList = array();

        while ($data = $response->fetch()) {

            array_push($itemIDList, $data["id"]);
        }


        foreach($itemIDList as $itemID) {

            $listeTickets = array();

            $query = "SELECT * from ticket WHERE item_id = ?";

            $sth =  $db->prepare($query);

            $sth->execute(array($itemID));

            while($data = $sth->fetch()) {
                array_push($listeTickets,$data["buyer_id"]." ".(string)$data["date"]);
            }

            shuffle($listeTickets);

            $winner = $listeTickets[0];


            // On met le vainqueur
            $query1 = "UPDATE item SET winner_id = :wid WHERE id = :id";
            $sth1 =  $db->prepare($query1);
            $sth1->execute(array(
                "wid"=>(int)$winner,
                "id"=>(int)$itemID
            ));


            // On supprime tous les tickets de l'objet

            $query = "DELETE from ticket WHERE item_id = :id";
            $sth =  $db->prepare($query);
            $sth->execute(array(
                "id"=>$itemID
            ));

        }

    }



}