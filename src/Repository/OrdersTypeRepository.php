<?php

namespace App\Repository;

use App\Entity\OrdersType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OrdersType|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdersType|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdersType[]    findAll()
 * @method OrdersType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrdersType::class);
    }

//    /**
//     * @return OrdersType[] Returns an array of OrdersType objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrdersType
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
