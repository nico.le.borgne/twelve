<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180928140626 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE order_bin (id INT AUTO_INCREMENT NOT NULL, item_id_id INT NOT NULL, seller_id_id INT NOT NULL, buyer_id_id INT NOT NULL, UNIQUE INDEX UNIQ_E97C158D55E38587 (item_id_id), INDEX IDX_E97C158DDF4C85EA (seller_id_id), INDEX IDX_E97C158D881D19F2 (buyer_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_bin ADD CONSTRAINT FK_E97C158D55E38587 FOREIGN KEY (item_id_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE order_bin ADD CONSTRAINT FK_E97C158DDF4C85EA FOREIGN KEY (seller_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE order_bin ADD CONSTRAINT FK_E97C158D881D19F2 FOREIGN KEY (buyer_id_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE order_bin');
    }
}
