<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181211154144 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64938248176');
        $this->addSql('DROP INDEX IDX_8D93D64938248176 ON user');
        $this->addSql('ALTER TABLE user ADD roles JSON NOT NULL, CHANGE country_id country_id INT NOT NULL, CHANGE mail mail VARCHAR(180) NOT NULL, CHANGE lastname lastname VARCHAR(255) NOT NULL, CHANGE currency_id currecy_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649FF706630 FOREIGN KEY (currecy_id) REFERENCES currency (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6495126AC48 ON user (mail)');
        $this->addSql('CREATE INDEX IDX_8D93D649FF706630 ON user (currecy_id)');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE8DE820D9');
        $this->addSql('DROP INDEX IDX_E52FFDEE8DE820D9 ON orders');
        $this->addSql('ALTER TABLE orders DROP seller_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders ADD seller_id INT NOT NULL');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE8DE820D9 FOREIGN KEY (seller_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E52FFDEE8DE820D9 ON orders (seller_id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FF706630');
        $this->addSql('DROP INDEX UNIQ_8D93D6495126AC48 ON user');
        $this->addSql('DROP INDEX IDX_8D93D649FF706630 ON user');
        $this->addSql('ALTER TABLE user DROP roles, CHANGE country_id country_id INT DEFAULT NULL, CHANGE mail mail VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE lastname lastname VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE currecy_id currency_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64938248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64938248176 ON user (currency_id)');
    }
}
