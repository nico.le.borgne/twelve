<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218131350 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE country ADD short_code VARCHAR(5) NOT NULL');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FF706630');
        $this->addSql('DROP INDEX IDX_8D93D649FF706630 ON user');
        $this->addSql('ALTER TABLE user DROP roles, CHANGE currecy_id currency_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64938248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64938248176 ON user (currency_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE country DROP short_code');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64938248176');
        $this->addSql('DROP INDEX IDX_8D93D64938248176 ON user');
        $this->addSql('ALTER TABLE user ADD roles JSON NOT NULL, CHANGE currency_id currecy_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649FF706630 FOREIGN KEY (currecy_id) REFERENCES currency (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649FF706630 ON user (currecy_id)');
    }
}
