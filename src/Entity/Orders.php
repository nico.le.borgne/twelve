<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Item", inversedBy="orders", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $buyer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Payment", mappedBy="orderPay")
     */
    private $payments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShippingMethodType", inversedBy="orders")
     */
    private $shipping_method;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $trackingNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrdersType", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ordersType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ticket_number;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getItemID(): ?Item
    {
        return $this->item;
    }

    public function setItemID(Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getSellerID(): ?User
    {
        return $this->seller;
    }

    public function setSellerID(?User $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getBuyerID(): ?User
    {
        return $this->buyer;
    }

    public function setBuyerID(?User $buyer): self
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setOrderPay($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getOrderPay() === $this) {
                $payment->setOrderPay(null);
            }
        }

        return $this;
    }

    public function getShippingMethod(): ?ShippingMethodType
    {
        return $this->shipping_method;
    }

    public function setShippingMethod(?ShippingMethodType $shipping_method): self
    {
        $this->shipping_method = $shipping_method;

        return $this;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->trackingNumber;
    }

    public function setTrackingNumber(?string $trackingNumber): self
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    public function getOrdersType(): ?OrdersType
    {
        return $this->ordersType;
    }

    public function setOrdersType(?OrdersType $ordersType): self
    {
        $this->ordersType = $ordersType;

        return $this;
    }

    public function getTicketNumber(): ?int
    {
        return $this->ticket_number;
    }

    public function setTicketNumber(?int $ticket_number): self
    {
        $this->ticket_number = $ticket_number;

        return $this;
    }
}
