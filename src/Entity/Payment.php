<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PaymentType", inversedBy="payments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $paymentType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Orders", inversedBy="payments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderPay;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cc_number;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cc_month;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cc_year;

    /**
     * @ORM\Column(type="integer")
     */
    private $cc_cvv;

    public function getId()
    {
        return $this->id;
    }

    public function getPaymentType(): ?PaymentType
    {
        return $this->paymentType;
    }

    public function setPaymentType(?PaymentType $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getOrderPay(): ?Orders
    {
        return $this->orderPay;
    }

    public function setOrderPay(?Orders $orderPay): self
    {
        $this->orderPay = $orderPay;

        return $this;
    }

    public function getCcNumber(): ?int
    {
        return $this->cc_number;
    }

    public function setCcNumber(?int $cc_number): self
    {
        $this->cc_number = $cc_number;

        return $this;
    }

    public function getCcMonth(): ?int
    {
        return $this->cc_month;
    }

    public function setCcMonth(?int $cc_month): self
    {
        $this->cc_month = $cc_month;

        return $this;
    }

    public function getCcYear(): ?int
    {
        return $this->cc_year;
    }

    public function setCcYear(?int $cc_year): self
    {
        $this->cc_year = $cc_year;

        return $this;
    }

    public function getCcCvv(): ?int
    {
        return $this->cc_cvv;
    }

    public function setCcCvv(int $cc_cvv): self
    {
        $this->cc_cvv = $cc_cvv;

        return $this;
    }
}
