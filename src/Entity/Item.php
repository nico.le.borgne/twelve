<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 * @Vich\Uploadable

 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $brand;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbre_tickets;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" })
     */
    private $image;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brand2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $size;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="boolean")
     */
    private $check_tickets;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $price_ticket;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sold;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Orders", mappedBy="item", cascade={"persist", "remove"})
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="items")
     */
    private $seller;

    /**
     * @ORM\Column(type="integer")
     */
    private $soldTickets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ticket", mappedBy="item")
     */
    private $tickets;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="itemWinner")
     */
    private $winner;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getname(): ?string
    {
        return $this->name;
    }

    public function setname(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getbrand(): ?string
    {
        return $this->brand;
    }

    public function setbrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getprice(): ?int
    {
        return $this->price;
    }

    public function setprice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getNbreTickets(): ?int
    {
        return $this->nbre_tickets;
    }

    public function setNbreTickets(int $nbre_tickets): self
    {
        $this->nbre_tickets = $nbre_tickets;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBrand2(): ?string
    {
        return $this->brand2;
    }

    public function setBrand2(?string $brand2): self
    {
        $this->brand2 = $brand2;

        return $this;
    }
    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCheckTickets(): ?bool
    {
        return $this->check_tickets;
    }

    public function setCheckTickets(bool $check_tickets): self
    {
        $this->check_tickets = $check_tickets;

        return $this;
    }

    public function getPriceTicket(): ?string
    {
        return $this->price_ticket;
    }

    public function setPriceTicket(string $price_ticket): self
    {
        $this->price_ticket = $price_ticket;

        return $this;
    }

    public function getSold(): ?bool
    {
        return $this->sold;
    }

    public function setSold(bool $sold): self
    {
        $this->sold = $sold;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOrder(): ?Orders
    {
        return $this->order;
    }

    public function setOrder(Orders $order): self
    {
        $this->order = $order;

        // set the owning side of the relation if necessary
        if ($this !== $order->getItemID()) {
            $order->setItemID($this);
        }

        return $this;
    }

    public function getSeller(): ?User
    {
        return $this->seller;
    }

    public function setSeller(?User $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getSoldTickets(): ?int
    {
        return $this->soldTickets;
    }

    public function setSoldTickets(int $soldTickets): self
    {
        $this->soldTickets = $soldTickets;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setItem($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->contains($ticket)) {
            $this->tickets->removeElement($ticket);
            // set the owning side to null (unless already changed)
            if ($ticket->getItem() === $this) {
                $ticket->setItem(null);
            }
        }

        return $this;
    }

    public function getWinner(): ?User
    {
        return $this->winner;
    }

    public function setWinner(?User $winner): self
    {
        $this->winner = $winner;

        return $this;
    }
}
