<?php

namespace App\Form;

use App\Entity\Item;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('brand',ChoiceType::class,[
                'choices'=>[
                    "Supreme"=>"Supreme",
                    "Palace"=>"Palace",
                    "Balenciaga"=>"Balenciaga",
                    "Off-White"=>"Off-White",
                    "Gucci"=>"Gucci",
                    "Dior"=>"Dior"
                ],
                'attr'=>[
                    'class'=>'form-control'
                ]
            ])
            ->add('brand2',ChoiceType::class,[
                'choices'=>[
                    "None"=>null,
                    "Supreme"=>"Supreme",
                    "Balenciaga"=>"Balenciaga",
                    "Off-White"=>"Off-White",
                    "Gucci"=>"Gucci",
                    "Dior"=>"Dior"
                ],
                'attr'=>[
                    'class'=>'form-control'
                ]
            ])            ->add('description')
            ->add('price',HiddenType::class,[
                'data'=>0
            ])
            ->add('nbre_tickets')
            ->add('image',FileType::class,array('data_class'=> null, 'label' => 'Image'))
            ->add('size')
            ->add('category')
            ->add('checkTickets',HiddenType::class, [
                'data'=>0
            ])
            ->add('priceTicket')
            ->add('sold',HiddenType::class,[
                'data'=>0
            ])
            ->add('date',HiddenType::class)
            ->add('soldTickets',HiddenType::class,[
                'data'=>0
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Item::class,
        ));
    }
}
