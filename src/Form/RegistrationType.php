<?php

namespace App\Form;

use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mail')
            ->add('password', PasswordType::class)
            ->add('plainPassword', PasswordType::class)
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
            ->add('address')
            ->add('postal')
            ->add('city')
            ->add('country',EntityType::class, [
                'class'=>Country::class,
                'choice_label'=>'name'
            ])
            ->add('currency',EntityType::class, [
                'class'=>Currency::class,
                'choice_label'=>'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
