<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profileIndex")
     */
    public function index()
    {
        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
        ]);
    }

    /**
     * @Route("/profile/modify", name="profileModify")
     */
    public function modifyInfos(Request $request)
    {

        $form = $this->createForm(RegistrationType::class, $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profileModify');

        }

        return $this->render('profile/modifyInfo.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profile/items", name="myItems")
     */
    public function myItems(Request $request) {
        return $this->render('profile/myOrders.html.twig', ['items'=>  $this->getUser()->getItems()]);

    }

    /**
     * @Route("/profile/raffles", name="myRaffles")
     */
    public function myRaffles(Request $request) {
        return $this->render('profile/raffles.html.twig', ['items'=>  $this->getUser()->getItemWinner(),'tickets'=>$this->getUser()->getTickets()]);

    }


}
