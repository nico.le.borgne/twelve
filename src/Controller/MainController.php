<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ItemRepository;


class MainController extends Controller
{

    /**
     * @Route("/", name="indexhome")
     */
    public function redirectMain() {
        return $this->redirectToRoute("home");
    }
    /**
     * @Route("/main", name="main")
     */
    public function index(ItemRepository $itemRepository)
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
            'items'=>$itemRepository->findAll()
        ]);
    }

    /**
     * @Route("/home", name="home")
     */
    public function home(ItemRepository $itemRepository)
    {
        return $this->render('main/index.html.twig', [ 'items' => $itemRepository->findLastItems(),'raffles' => $itemRepository->findLastRaffles() ]);
    }
}
